extends Control


signal btn_spin_pressed
signal btn_autospin_pressed


var FOOTER_LAYOUT_POS: float # footer layout position.y
var AUTOSPIN_PROGRESS_POS: float


# меняется сигналами из игры
var spinning: bool = false
var auto_spinning: bool = false


const SPIN_TIME: float = 2.3 # время кручения после разгона
var AUTO_SPIN_COUNT: int = 1 # счетчик количества повторов автоспина
const AUTO_SPIN_MAX: float = 10.0 # количество повторов автоспина

var SCORE_WIN: int = 0 setget set_SCORE_WIN # счетчик выигранных очков

const MIN_BET: float = 10.0 # минимальный порог ставки
const MAX_BET: float = 200.0 # максимальный порог ставки
const BET_STEP: float= 10.0 # шаг инкремента/дикремента
var BET: float= 100.0 # текущий коэффициент ставки

const TWEEN_SPEED: float = 0.3
var _t: bool


# BUILTINS -------------------------


func _ready() -> void:
	if not ($FooterLayout/Bet as Control).is_visible_in_tree() and not ($FooterLayout/BtnMaxBet as Button).is_visible_in_tree():
		BET = MAX_BET
	FOOTER_LAYOUT_POS = ($FooterLayout as Control).get_rect().position.y
	AUTOSPIN_PROGRESS_POS = ($AutoSpinProgress as TextureProgress).get_rect().position.y
	($FooterLayout/Bet/Label as Label).text = str(BET)
	($AutoSpinProgress as TextureProgress).min_value = 0.0
	($AutoSpinProgress as TextureProgress).max_value = AUTO_SPIN_MAX
	($AutoSpinProgress as TextureProgress).step = AUTO_SPIN_MAX / 500.0
	set_SCORE_WIN(0)


# METHODS -------------------------


# установка очков
func add_score(count: int) -> void:
	_t = ($Tween as Tween).interpolate_method(self, "set_SCORE_WIN", SCORE_WIN, SCORE_WIN + count, TWEEN_SPEED)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()


# отображение автоспина
func show_hide_autospin_progress(show: bool) -> void:
	var footer_size: float = self.get_rect().size.y
	var progress_pos_show: float = (footer_size - ($AutoSpinProgress as TextureProgress).get_rect().size.y) / 2
	if show:
		_t = ($Tween as Tween).interpolate_property($AutoSpinProgress, "rect_position:y", AUTOSPIN_PROGRESS_POS, progress_pos_show, TWEEN_SPEED)
		_t = ($Tween as Tween).interpolate_property($FooterLayout, "rect_position:y", FOOTER_LAYOUT_POS, FOOTER_LAYOUT_POS + footer_size, TWEEN_SPEED)
		_t = ($Tween as Tween).interpolate_property($AutoSpinProgress, "modulate:a", 0.0, 1.0, TWEEN_SPEED)
		_t = ($Tween as Tween).interpolate_property($FooterLayout, "modulate:a", 1.0, 0.0, TWEEN_SPEED)
	else:
		_t = ($Tween as Tween).interpolate_property($AutoSpinProgress, "rect_position:y", progress_pos_show, AUTOSPIN_PROGRESS_POS, TWEEN_SPEED)
		_t = ($Tween as Tween).interpolate_property($FooterLayout, "rect_position:y", FOOTER_LAYOUT_POS + footer_size, FOOTER_LAYOUT_POS, TWEEN_SPEED)
		_t = ($Tween as Tween).interpolate_property($AutoSpinProgress, "modulate:a", 1.0, 0.0, TWEEN_SPEED)
		_t = ($Tween as Tween).interpolate_property($FooterLayout, "modulate:a", 0.0, 1.0, TWEEN_SPEED)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()
	($AutoSpinProgress as TextureProgress).value = 0


# изменеие счетчика количества и прогресса автоспинов
func change_autospin_count() -> void:
	var progress: TextureProgress = ($AutoSpinProgress as TextureProgress)
	var progress_value: float = progress.value
	_t = ($Tween as Tween).interpolate_property(progress, "value", progress_value, AUTO_SPIN_COUNT, SPIN_TIME + 1.5)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()
	($AutoSpinProgress/Label as Label).text = "%s / %s" % [AUTO_SPIN_COUNT, AUTO_SPIN_MAX]


func set_autospin_count(count: int) -> void:
	AUTO_SPIN_COUNT = count


# SETGET -------------------------


func set_SCORE_WIN(num: int) -> void:
	SCORE_WIN = num
	if ($FooterLayout/BgWin/ScoreWin as Label).is_visible_in_tree():
		($FooterLayout/BgWin/ScoreWin as Label).text = "$ %s" % Global.call("format_number", num as float)


# SIGNALS -------------------------


func _on_BtnSpin_button_down() -> void:
	($TimerSpinLongPress as Timer).start()


func _on_BtnSpin_button_up() -> void:
	if not spinning and not auto_spinning:
		emit_signal("btn_spin_pressed")
		($TimerSpinLongPress as Timer).stop()


func _on_TimerSpinLongPress_timeout() -> void:
	if not spinning and not auto_spinning:
		emit_signal("btn_autospin_pressed")
		show_hide_autospin_progress(true)
		change_autospin_count()


func _on_BtnMinus_pressed() -> void:
	if not spinning and not auto_spinning:
		BET = BET - BET_STEP if BET > MIN_BET else MIN_BET
		($FooterLayout/Bet/Label as Label).text = str(BET)
		_t = ($Tween as Tween).interpolate_property($FooterLayout/Bet/BtnMinus, "modulate:a", null, 1.0, TWEEN_SPEED)
		_t = ($Tween as Tween).interpolate_property($FooterLayout/Bet/BtnPlus, "modulate:a", null, 1.0, TWEEN_SPEED)
		if BET == MIN_BET:
			_t = ($Tween as Tween).interpolate_property($FooterLayout/Bet/BtnMinus, "modulate:a", null, 0.0, TWEEN_SPEED)
		if not ($Tween as Tween).is_active():
			_t = ($Tween as Tween).start()


func _on_BtnPlus_pressed() -> void:
	if not spinning and not auto_spinning:
		BET = BET + BET_STEP if BET < MAX_BET else MAX_BET
		($FooterLayout/Bet/Label as Label).text = str(BET)
		_t = ($Tween as Tween).interpolate_property($FooterLayout/Bet/BtnPlus, "modulate:a", null, 1.0, TWEEN_SPEED)
		_t = ($Tween as Tween).interpolate_property($FooterLayout/Bet/BtnMinus, "modulate:a", null, 1.0, TWEEN_SPEED)
		if BET == MAX_BET:
			_t = ($Tween as Tween).interpolate_property($FooterLayout/Bet/BtnPlus, "modulate:a", null, 0.0, TWEEN_SPEED)
		if not ($Tween as Tween).is_active():
			_t = ($Tween as Tween).start()


func _on_BtnAutoSpin_pressed() -> void:
	if not spinning and not auto_spinning:
		emit_signal("btn_autospin_pressed")
		show_hide_autospin_progress(true)
		change_autospin_count()


func _on_BtnMaxBet_pressed() -> void:
	if not spinning and not auto_spinning:
		BET = MAX_BET
		($FooterLayout/Bet/Label as Label).text = str(BET)
		var btn_max_opacity: float = ($FooterLayout/Bet/BtnPlus as Button).modulate.a
		var btn_min_opacity: float = ($FooterLayout/Bet/BtnMinus as Button).modulate.a
		_t = ($Tween as Tween).interpolate_property($FooterLayout/Bet/BtnPlus, "modulate:a", btn_max_opacity, 0.0, TWEEN_SPEED)
		_t = ($Tween as Tween).interpolate_property($FooterLayout/Bet/BtnMinus, "modulate:a", btn_min_opacity, 1.0, TWEEN_SPEED)
		if not ($Tween as Tween).is_active():
			_t = ($Tween as Tween).start()


