extends Control


# описание видимости блоков Header
const HEADER_SETUP: Dictionary = {
	"is_header_visible": true,
	"is_score_visible": true,
	"is_level_visible": true,
	"is_btn_back_visible": true,
	"is_bg_visible": false,
}

enum ROUND {
	IDLE,
	PROCEED,
	ENDED,
}
var _round: int = ROUND.IDLE

const C_GREEN: Color = Color(0, 1, 0.423529)
const C_RED: Color = Color(0.8078431373, 0.01960784314, 0.1137254902)

const MIN_BET: float = 10.0 # минимальный порог ставки
const MAX_BET: float = 200.0 # максимальный порог ставки
const BET_STEP: float = 10.0 # шаг инкремента/дикремента
var BET: float = 100.0 setget set_BET # текущий коэффициент ставки

var MAX_COEF: float = 10.0
const COEF_STEP: float = 0.03
var COEF: float = 1.0

const TWEEN_SPEED: float = 0.3
var _t: bool


# BUILTINS -------------------------


func _ready() -> void:
	($AnimationPlayer as AnimationPlayer).play("fly")
	($Footer/BtnStop as Button).hide()
	set_BET(BET)
	set_COEF(COEF)
	($Coef as Label).modulate = C_GREEN
	if not ($Footer/Bet as Control).is_visible_in_tree() and not ($Footer/BtnMaxBet as Button).is_visible_in_tree():
		BET = MAX_BET


# METHODS -------------------------


func start_flying() -> void:
	($Coef as Label).modulate = C_GREEN
	set_COEF(1.0)
	($Footer/BtnSpin as Button).hide()
	($Footer/BtnStop as Button).show()
	($Timer as Timer).start()
	MAX_COEF = randf() * 10.0


func stop_flying() -> void:
	($Footer/BtnSpin as Button).show()
	($Footer/BtnStop as Button).hide()
	($Timer as Timer).stop()
	if _round == ROUND.PROCEED:
		get_node("/root/Main/Header").call_deferred("change_score", BET * COEF, 0)
	else:
		get_node("/root/Main/Header").call_deferred("change_score", BET * COEF, 1, false)
	_round = ROUND.IDLE


# SETGET -------------------------


func set_BET(bet: float) -> void:
	BET = bet
	($Footer/Bet/Label as Label).text = Global.call("format_number", BET) + "C"


func set_COEF(coef: float) -> void:
	COEF = coef
	var coef_string: String = str(coef) + "x"
	($Coef as Label).text = coef_string


# SIGNALS -------------------------


func _on_BtnSpin_pressed() -> void:
	_round = ROUND.PROCEED
	start_flying()


func _on_BtnStop_pressed() -> void:
	stop_flying()


func _on_BtnMinus_pressed() -> void:
	set_BET(BET - BET_STEP if BET > MIN_BET else MIN_BET)
	_t = ($Tween as Tween).interpolate_property($Footer/Bet/BtnMinus, "modulate:a", null, 1.0, TWEEN_SPEED)
	_t = ($Tween as Tween).interpolate_property($Footer/Bet/BtnPlus, "modulate:a", null, 1.0, TWEEN_SPEED)
	if BET == MIN_BET:
		_t = ($Tween as Tween).interpolate_property($Footer/Bet/BtnMinus, "modulate:a", null, 0.0, TWEEN_SPEED)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()


func _on_BtnPlus_pressed() -> void:
	set_BET(BET + BET_STEP if BET < MAX_BET else MAX_BET)
	_t = ($Tween as Tween).interpolate_property($Footer/Bet/BtnPlus, "modulate:a", null, 1.0, TWEEN_SPEED)
	_t = ($Tween as Tween).interpolate_property($Footer/Bet/BtnMinus, "modulate:a", null, 1.0, TWEEN_SPEED)
	if BET == MAX_BET:
		_t = ($Tween as Tween).interpolate_property($Footer/Bet/BtnPlus, "modulate:a", null, 0.0, TWEEN_SPEED)
	if not ($Tween as Tween).is_active():
		_t = ($Tween as Tween).start()


func _on_Timer_timeout() -> void:
	set_COEF(COEF + COEF_STEP)
	if COEF > MAX_COEF:
		_round = ROUND.ENDED
		($Coef as Label).modulate = C_RED
		stop_flying()


